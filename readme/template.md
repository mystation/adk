![MyStation logo](<%= appIconUrl %>)

# MyStation app : <%= appConfig.name %>

[![mystation](https://img.shields.io/static/v1?label=MyStation&labelColor=191919&message=since%20v<%= appConfig.mystation_version %>&color=3D373A&logo=data:image/png;base64,AAABAAEAEBAAAAEAIABoBAAAFgAAACgAAAAQAAAAIAAAAAEAIAAAAAAAAAQAABILAAASCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADTOpAA0zqQMGLK1AiblVxIe4VsAFKq85DDKqAg0zqQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANM6kADTOpAA0zqSUNM6mNCzGqx4e3VtqAsVvXCzCrxw0zqYMNM6keDTOpAA0zqQAAAAAAAAAAAA40qAAMM6kADTOpFg0zqXQNM6nKDTOpnAQpryy+9DGRvPIyggAlsjENM6miDTOpxQ0zqWcNM6kQDTOpAA81pgCSvQAFQmpiUxE4o8ANM6m0DTOpRg0zqQan2kEAv/UwkL/1MICs4D0ADTOpBw0zqUwNM6mzEjijtElxWUa24gADfqcUIHOdIs40XHVxAB7EDQ0zqQCo3D8Av/UwAL/1MJC/9TCAv/UwAA40qQANM6kDCi+uVyNJjOtxmiXYgqwOIX+pEiN/qRLEgKoRKICqEQAAAAAAv/UwAL/1MAC/9TCQv/UwgP//AAANM6ktDTOpmwwxq8wtVH+ZfacVzYCqESd/qRIjf6kSxH+pEih/qRIAAAAAAL/1MAC/9TAAwPYvj6XYQpcGK655DTOpzw0zqZkHLLIpfacVJ3+pEsV/qRInf6kSI3+pEsR/qRIof6kSALHlNQDB+DAAvPIyEsL4LquKu1T5HUWevQMosEAAG7gEcJklAH+pEid/qRLGf6kSJ3+pEiN/qRLEf6kSKIm1FwC78TEIvvQxT770ML+88S/Pue4y0rnuNMPG/CtJx/4oB4q1FQB/qRInf6kSxn+pEid/qRIjfqgSw4GrEyrB9zIzvvQwp7zxMNi26zCEqtwvHbbqKhq88i5+vvQv07/0MKPB9zExgasTKH6oEsV/qRInf6kSIou3F8y26imnvvQw17jtMaGu4jEwfqs3ApfHNACu4SgAn9AhAbnuLSy98i+ZwPYv0rbqKKOKthfOf6kSJ53MJR+r3jHamMtH6JnLRFTf/wgEvu4jAAAAAAAAAAAAAAAAAAAAAADR+BwA//8AA5vNRVGZy0jnq94w3JnIICLE+BMFZJFnWRpBn8UJL6yrDDKqOw40qAMNM6kAAAAAAAAAAAANM6kADjSoBAwyqkMJL6uxG0KewWqYYlPR/wAEARDOACJldgAMMascDTOpgw0zqc8NM6mNDTOpIg0zqQARN6YADTOpJw0zqZYNM6nPDTOpfAwwqxgYTpAAAA3NAAAAAAAAAAAADTOpAA0zqQINM6kyDTOpog0zqcdgjW52W4hxfA0zqcoNM6mcDTOpLQ0zqQENM6kAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADTOpAAowqwkPNadbjsBP24/ATdcPNaZUCS+tBw0zqQAAAAAAAAAAAAAAAAAAAAAA+B8AAOAPAADAAwAAAkAAAA5gAAAeQAAAHgAAABwIAAAQCAAAAAAAAAGAAAAH4AAAA8AAAMEDAADgBwAA+B8AAA==)](http://mystation.fr)
[![<%= appConfig.name %>](https://img.shields.io/static/v1?labelColor=<%= appColor %>&label=<%= appConfig.name %>&message=v<%= appConfig.version %>&color=191919)](<%= projectUrl %>)
[![pipeline status](<%= projectUrl %>badges/master/pipeline.svg)](<%= projectUrl %>commits/master)

## Description

_<%= appConfig.title %>_

__Version:__ `<%= appConfig.version %>`

__MyStation version:__ `<%= appConfig.mystation_version %>`

🌎 Translated
<%= translationsList %>

## Dependencies

<details>
<summary>App dependencies</summary>

<%= dependencies %>

</details>