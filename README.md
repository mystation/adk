# MyStation App Development Kit

[![mystation](https://img.shields.io/static/v1?label=&message=MyStation&color=191919&logo=data:image/png;base64,AAABAAEAEBAAAAEAIABoBAAAFgAAACgAAAAQAAAAIAAAAAEAIAAAAAAAAAQAABILAAASCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADTOpAA0zqQMGLK1AiblVxIe4VsAFKq85DDKqAg0zqQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANM6kADTOpAA0zqSUNM6mNCzGqx4e3VtqAsVvXCzCrxw0zqYMNM6keDTOpAA0zqQAAAAAAAAAAAA40qAAMM6kADTOpFg0zqXQNM6nKDTOpnAQpryy+9DGRvPIyggAlsjENM6miDTOpxQ0zqWcNM6kQDTOpAA81pgCSvQAFQmpiUxE4o8ANM6m0DTOpRg0zqQan2kEAv/UwkL/1MICs4D0ADTOpBw0zqUwNM6mzEjijtElxWUa24gADfqcUIHOdIs40XHVxAB7EDQ0zqQCo3D8Av/UwAL/1MJC/9TCAv/UwAA40qQANM6kDCi+uVyNJjOtxmiXYgqwOIX+pEiN/qRLEgKoRKICqEQAAAAAAv/UwAL/1MAC/9TCQv/UwgP//AAANM6ktDTOpmwwxq8wtVH+ZfacVzYCqESd/qRIjf6kSxH+pEih/qRIAAAAAAL/1MAC/9TAAwPYvj6XYQpcGK655DTOpzw0zqZkHLLIpfacVJ3+pEsV/qRInf6kSI3+pEsR/qRIof6kSALHlNQDB+DAAvPIyEsL4LquKu1T5HUWevQMosEAAG7gEcJklAH+pEid/qRLGf6kSJ3+pEiN/qRLEf6kSKIm1FwC78TEIvvQxT770ML+88S/Pue4y0rnuNMPG/CtJx/4oB4q1FQB/qRInf6kSxn+pEid/qRIjfqgSw4GrEyrB9zIzvvQwp7zxMNi26zCEqtwvHbbqKhq88i5+vvQv07/0MKPB9zExgasTKH6oEsV/qRInf6kSIou3F8y26imnvvQw17jtMaGu4jEwfqs3ApfHNACu4SgAn9AhAbnuLSy98i+ZwPYv0rbqKKOKthfOf6kSJ53MJR+r3jHamMtH6JnLRFTf/wgEvu4jAAAAAAAAAAAAAAAAAAAAAADR+BwA//8AA5vNRVGZy0jnq94w3JnIICLE+BMFZJFnWRpBn8UJL6yrDDKqOw40qAMNM6kAAAAAAAAAAAANM6kADjSoBAwyqkMJL6uxG0KewWqYYlPR/wAEARDOACJldgAMMascDTOpgw0zqc8NM6mNDTOpIg0zqQARN6YADTOpJw0zqZYNM6nPDTOpfAwwqxgYTpAAAA3NAAAAAAAAAAAADTOpAA0zqQINM6kyDTOpog0zqcdgjW52W4hxfA0zqcoNM6mcDTOpLQ0zqQENM6kAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADTOpAAowqwkPNadbjsBP24/ATdcPNaZUCS+tBw0zqQAAAAAAAAAAAAAAAAAAAAAA+B8AAOAPAADAAwAAAkAAAA5gAAAeQAAAHgAAABwIAAAQCAAAAAAAAAGAAAAH4AAAA8AAAMEDAADgBwAA+B8AAA==)](http://mystation.fr)
[![mystation-adk](https://img.shields.io/static/v1?labelColor=3D373A&label=mystation-adk&message=v1.0.0&color=191919)](https://gitlab.com/mystation/mystation-adk/)
[![Discord](https://img.shields.io/discord/729028505123291228?label=Discord&logo=discord&logoColor=white)](https://discordapp.com/channels/729028505123291228)
[![pipeline status](https://gitlab.com/mystation/mystation-adk/badges/master/pipeline.svg)](https://gitlab.com/mystation/mystation-adk/commits/master)

> Please consult [documentation](http://developers.mystation.fr/)

## Get started

Clone this [repository](https://gitlab.com/mystation/mystation-adk/) and install dependencies `npm install`

## Development

```bash
# Clone this repository
git clone https://gitlab.com/mystation/mystation-adk.git
# Install packages
npm install
# Take a coffee ;)
```

## Start hot development server

```bash
npm run dev
```

## Build

```bash
# Build
npm run build
```