/**
 * Update app
 */

__MYSTATION__.updateApp(__APP_NAME__, function (resources, fromVersion) {
  __MYSTATION__.log('info', 'Update ' + __APP_NAME__ + ' from version : ' + fromVersion, 'console')
  // Return resources object
  return resources
})
